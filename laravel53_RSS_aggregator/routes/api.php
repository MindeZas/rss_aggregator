<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
*/
Route::group(
    [ 'middleware' => [ 'auth' ] ],
    function(){
    Route::get('categories',[
        'uses' => 'CategoryController@getAdminCategories',
        'as' => 'categories'
    ]);

    Route::post('create-category',[
        'uses' => 'CategoryController@createCategory',
        'as' => 'category.create'
    ]);

    Route::get('delete-category/{id}',[
        'uses' => 'CategoryController@deleteCategory',
        'as' => 'category.delete'
    ]);

    Route::get('edit-category/{id}',[
        'uses' => 'CategoryController@editCategory',
        'as' => 'category.edit'
    ]);

    Route::put('update-category/{id}',[
        'uses' => 'CategoryController@updateCategory',
        'as' => 'category.update'
    ]);

    Route::get('category/{id}/articles', [
        'uses' => 'CategoryController@getCategoryArticles',
        'as' => 'category.articles'
    ]);

    Route::post('create-article',[
        'uses' => 'ArticleController@createArticle',
        'as' => 'article.create'
    ]);

    Route::get('delete-article/{id}',[
        'uses' => 'ArticleController@deleteArticle',
        'as' => 'article.delete'
    ]);

    Route::get('edit-article/{id}',[
        'uses' => 'ArticleController@editArticle',
        'as' => 'article.edit'
    ]);

    Route::put('update-article/{id}',[
        'uses' => 'ArticleController@updateArticle',
        'as' => 'article.update'
    ]);
});



  Route::get('/signin', function () {
      return view('admin-views.signin');
  })->name('signin');

  Route::get('/signup', function () {
      return view('admin-views.signup');
  })->name('signup');

  Route::post('/signup',[
    'uses' => 'UserController@postSignUp',
    'as' => 'signup'
  ]);

  Route::post('/signin',[
    'uses' => 'UserController@postSignIn',
    'as' => 'signin'
  ]);

  Route::get('/logout',[
    'uses' => 'UserController@getLogout',
    'as' => 'logout',
    'middleware' => 'auth'
  ]);

  Route::get('/account',[
    'uses' => 'UserController@getAccount',
    'as' => 'account',
    'middleware' => 'auth'
  ]);

  Route::put('/updateaccount',[
    'uses' => 'UserController@putSaveAccount',
    'as' => 'account.save'
  ]);
