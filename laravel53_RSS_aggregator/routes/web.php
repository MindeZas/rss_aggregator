<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/',[
    'uses' => 'CategoryController@getUserCategories',
    'as' => 'news'
]);

Route::get('category/{id}',[
    'uses' => 'CategoryController@viewCategory',
    'as' => 'category'
]);

