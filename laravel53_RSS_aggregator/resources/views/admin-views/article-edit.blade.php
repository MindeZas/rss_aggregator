@extends('admin-views.admin-layout.admin-main')
@section('title')
categories
@endsection
@section('content')
  <div class="row col-sm-4">
    <div class="col-sm-12">
     <div class="form-group row" style="margin-top: 20px;">
     <div class="row" style="margin: 0 20px;">
      @include('admin-views.admin-partials.message-block') 
      </div>
          {!! Form::open(array('route'=>['article.update', $article->id],'method'=>'PUT','class'=>'col-sm-12')) !!}
            {!! Form::label('name', 'Article name:', array('class'=>'col-sm-12 control-label'))!!}
            {!! Form::text('name', $article->name, array('class'=>'form-control form-control')) !!}
            <br>
            {!! Form::label('url', 'Article url:', array('class'=>'col-sm-12 control-label'))!!}
            {!! Form::textarea('url', $article->url, array('class'=>'form-control form-control', 'rows'=>'1')) !!}
            <br>
            <a class="btn btn-default" href="{{route('category.articles',['id'=>$article->category_id])}}">Cancel</a>
            {!! Form::submit('Update', array('class'=>'btn btn-success col-sm-6 pull-right')) !!}
         {!! Form::close() !!}
      </div>  
  </div><!-- end col-sm-12 -->
</div><!--end row col-sm-4 -->
@endsection