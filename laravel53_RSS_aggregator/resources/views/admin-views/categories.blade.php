@extends('admin-views.admin-layout.admin-main')
@section('title')
categories
@endsection
@section('content')
<div class="row col-sm-12">
@include('admin-views.admin-partials.message-block') 
  <div class="row col-sm-4">
    <div class="col-sm-12">
     <div class="form-group row" style="margin-top: 20px;">
     <div class="row" style="margin: 0 20px;">
        </div>
          {!! Form::open(array('route'=>'category.create','method'=>'POST','class'=>'col-sm-12')) !!}
            {!! Form::label('name', 'Category name:', array('class'=>'col-sm-12 control-label'))!!}
            {!! Form::text('name', null, array('class'=>'form-control form-control','placeholder'=>'max 30 characters')) !!}
            <br>
            <a class="btn btn-default" href="{{route('categories')}}"> Cancel / refresh </a>
            {!! Form::submit('Add', array('class'=>'btn btn-success col-sm-6 pull-right')) !!}
         {!! Form::close() !!}
      </div> 
      <table class="table table-bordered" style="margin-top: 20px;">
        <thead>
          <tr>
            <th>id</th>
            <th colspan="2"> Categories </th>
            <th colspan="3" class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
              <?php $i=true; $y=0; ?>
            @foreach($category->articles as $article)
              <?php $i=false; $y++; ?>
            @endforeach
          <tr>
            <td>{{$category->id}}</td>
            <td><a href="{{route('category.articles',['id'=>$category->id])}}">{{$category->name}} >> <span class="btn btn-default pull-right">{{$y}}</span></a></td>
            <td> <a class="btn btn-primary" href="{{route('category.edit',['id'=>$category->id])}}">edit</a></td>

            <td>
              @if($i)
                <a class="btn btn-danger" href="{{route('category.delete',['id'=>$category->id])}}">X</a>
              @else
                <button type="button" class="btn btn-outline-secondary" data-toggle="tooltip" title="{{$category->name}} not empty! ">X</button>
              @endif
            </td>
          </tr>
        @endforeach
        </tbody>
      </table>
    </div><!-- end col-sm-12 -->
  </div><!--end row col-sm-4 -->
  <div class="row col-sm-8" style="border:0px solid #111;">
  @include('admin-views.articles-list') 
  </div>
</div>
@endsection
