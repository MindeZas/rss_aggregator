@extends('admin-views.admin-layout.admin-main')
@section('title')
categories
@endsection
@section('content')
  <div class="row col-sm-4">
    <div class="col-sm-12">
     <div class="form-group row" style="margin-top: 20px;">
     <div class="row" style="margin: 0 20px;">
      @include('admin-views.admin-partials.message-block') 
      </div>
          {!! Form::open(array('route'=>['category.update', $category->id],'method'=>'PUT','class'=>'col-sm-12')) !!}
            {!! Form::label('name', 'Category name:', array('class'=>'col-sm-12 control-label'))!!}
            {!! Form::text('name', $category->name, array('class'=>'form-control form-control','placeholder'=>'max 30 characters')) !!}
            <br>
            <a class="btn btn-default" href="{{route('categories')}}">Cancel</a>
            {!! Form::submit('Update', array('class'=>'btn btn-success col-sm-6 pull-right')) !!}
         {!! Form::close() !!}
      </div>  
  </div><!-- end col-sm-12 -->
</div><!--end row col-sm-4 -->
@endsection