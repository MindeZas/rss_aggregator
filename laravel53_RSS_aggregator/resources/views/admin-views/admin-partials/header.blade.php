<header>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    @if(Auth::check())
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="{{route('account')}}">Account</a></li>
        <li><a href="{{route('logout')}}">Logout</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
    @else
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="{{route('signin')}}">Signin</a></li>
        <li><a href="{{route('signup')}}">Signup</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
    @endif
  </div><!-- /.container-fluid -->
</nav>
</header