    @if(count($errors) > 0)
            <div class="col-md-12 text-center" style="background:#e99; ">
                <ul style="list-style-type: none; margin:0;">
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
    @endif
    @if(Session::has('message'))
            <div class="col-md-12 text-center" style="background:#9e9;">
                {{Session::get('message')}}
            </div>
    @endif