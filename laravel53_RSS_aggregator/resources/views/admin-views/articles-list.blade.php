<h4>Articles list</h4> 
<table class="table table-bordered" style="margin-top: 20px;">
  <thead>
    <tr>
      <th>Category</th>
      <th>Article/provider</th>
      <th width="70%">URL</th>
      <th colspan="2" class="text-center">Action</th>
    </tr>
  </thead>
  <tbody>
  @foreach($categories as $category)
    @foreach($category->articles as $article)
     <tr>
      <td>{{$category->name}}</td>
        <td>{{$article->name}}</td>
        <td width="70%">{{$article->url}}</td>
        <td><a class="btn btn-primary" href="{{route('article.edit',['id'=>$article->id])}}">Edit</a></td>
        <td><a class="btn btn-danger" href="{{route('article.delete',['id'=>$article->id])}}" data-toggle="tooltip" title="delete article" onclick="return ConfirmDelete()">X</a></td>
      </tr>
   @endforeach
  @endforeach
  </tbody>
</table>