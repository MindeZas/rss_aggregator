@extends('admin-views.admin-layout.admin-main')
@section('title')
categories
@endsection
@section('content')
  <div class="row col-sm-8 pull-right">
    <div class="col-sm-12">
     <div class="form-group row" style="margin-top: 10px;">
     <div class="row" style="margin: 0 20px;">
      @include('admin-views.admin-partials.message-block') 
     </div>
         {!! Form::open(array('route'=>'article.create','method'=>'POST','class'=>'form-horizontal')) !!}
         	<div class="col-sm-6"> 
            {!! Form::label('name', 'Article name:', array('class'=>'control-label'))!!}
            {!! Form::text('name', null, array('class'=>'form-control','placeholder'=>'max 30 characters')) !!}
           </div>
           <div class="col-sm-2"> 
            {!! Form::label('category_id', 'Category id:', array('class'=>'control-label'))!!}
            {!! Form::text('category_id', $category->id, array('class'=>'form-control','readonly')) !!}
           </div>
          <div class="col-sm-4"> 
            {!! Form::label('category_name', 'Category name:', array('class'=>'control-label'))!!}
            {!! Form::text('category_name', $category->name, array('class'=>'form-control','readonly')) !!}
           </div>
           <div class="col-sm-12" style="margin-top: 10px;"> 
            {!! Form::label('url', 'URL:', array('class'=>'control-label'))!!}
            {!! Form::textarea('url', null, array('class'=>'form-control', 'rows'=>'1','placeholder'=>'paste the url here')) !!}
            </div>
            <br>
            <div class="col-sm-12" style="margin-top: 10px;"> 
            <a class="btn btn-default col-sm-3" href="{{route('category.articles',['id'=>$category->id])}}">Cancel / refresh</a>
            {!! Form::submit('Add', array('class'=>'btn btn-success col-sm-6 pull-right')) !!}
            </div>
         {!! Form::close() !!}
      </div> 
      <table class="table table-bordered" style="margin-top: 20px;">
        <thead>
          <tr>
            <th>id</th>
            <th>Name</th>
            <th width="70%">URL</th>
            <th colspan="2" class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
	        @foreach($category->articles as $article)
	          <tr>
	            <td>{{$article->id}}</td>
	            <td>{{$article->name}}</td>
	            <td width="70%">{{$article->url}}</td>
              <td><a class="btn btn-primary" href="{{route('article.edit',['id'=>$article->id])}}">Edit</a></td>
              <td><a class="btn btn-danger" href="{{route('article.delete',['id'=>$article->id])}}" data-toggle="tooltip" title="delete article" onclick="return ConfirmDelete()">X</a></td>
            </tr>
			@endforeach
        </tbody>
      </table>
  </div><!-- end col-sm-12 -->
</div><!--end row col-sm-8 -->
  <div class="row col-sm-4" style="border:0px solid #111;">
  @include('admin-views.categories-list') 
  </div>
@endsection
