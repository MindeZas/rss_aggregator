@extends('admin-views.admin-layout.admin-main')
@section('title')
    Wellcome!
@endsection
@section('content')
@include('admin-views.admin-partials.message-block')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h3> Sign In </h3>
             {!! Form::open(array('route'=>'signin','method'=>'POST','class'=>'col-sm-12')) !!}
                {!! Form::label('email', 'E-mail:', array('class'=>'col-sm-12 control-label'))!!}
                {!! Form::text('email',null, array('class'=>'form-control form-control','placeholder'=>'your email')) !!}
                {!! Form::label('password', 'password:', array('class'=>'col-sm-12 control-label'))!!}
                {!! Form::password('password', array('placeholder'=>'Password', 'class'=>'form-control'))!!}<br>
                {!! Form::submit('Submit', array('class'=>'btn btn-primary col-sm-4')) !!}
             {!! Form::close() !!}
        </div>
    </div>
@endsection