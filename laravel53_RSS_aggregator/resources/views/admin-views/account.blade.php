@extends('admin-views.admin-layout.admin-main')
@section('title')
    Wellcome!
@endsection
@section('content')
@include('admin-views.admin-partials.message-block')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
         {!! Form::open(array('route'=>'account.save','method'=>'PUT','class'=>'col-sm-12')) !!}
            {!! Form::label('name', 'Name:', array('class'=>'col-sm-12 control-label'))!!}
            {!! Form::text('name', $user->name, array('class'=>'form-control form-control','placeholder'=>'max 30 characters')) !!}
            {!! Form::label('password', 'password:', array('class'=>'col-sm-12 control-label'))!!}
            {!! Form::password('password', array('placeholder'=>'Password', 'class'=>'form-control'))!!}
            <br>
            <a class="btn btn-default" href="{{route('categories')}}">Cancel</a>
            {!! Form::submit('Update user name / password', array('class'=>'btn btn-success col-sm-6 pull-right')) !!}
         {!! Form::close() !!}
        </div>
    </div>
@endsection
