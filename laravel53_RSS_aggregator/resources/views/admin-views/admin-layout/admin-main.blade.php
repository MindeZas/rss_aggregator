@include('admin-views.admin-partials.head')   
@include('admin-views.admin-partials.header')   
	<body>
		<div class="container" style="margin:0;padding:0;">
			@yield('content')
		</div><!-- end of .container --> 
		@include('admin-views.admin-partials.scripts')
	</body>
</html>