      <table class="table table-bordered" style="margin-top: 20px;padding-left: 40px;">
        <thead>
          <tr>
            <th style="padding-left: 40px;"><a class="btn btn-default" href="{{route('categories')}}">Back to categories</a></th>
          </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
          <tr>
            <td style="padding-left: 40px;"><a href="{{route('category.articles',['id'=>$category->id])}}">{{$category->name}}</a></td>
          </tr>
        @endforeach
        </tbody>
      </table>