@include('user-views.user-partials.head')   
  <body">
    <div class="container">
      @yield('content')
    </div><!-- end of .container --> 
    @include('user-views.user-partials.scripts')
  </body>
</html>