@extends('user-views.user-layout.user-main')
@section('title')
    Wellcome!
@endsection
@section('content')
<body>
<div class="row">
	<div class="col-xs-2">
	@include('user-views.user-partials.nav')<!---->
	</div>
	<div class="col-xs-10">
		<table class="table table-bordered">
		<tr><td><b>provider</b></td><td><b>title</b></td><!--<td><b>description</b></td>--><td><b>date</b></td></tr>
			@if(isset($category->id))
					@include('user-views.articleFeed')
			@elseif(isset($categories)) 
				@foreach($categories as $category)
					@include('user-views.articleFeed')
		    	@endforeach
		    @endif
		</table>
	</div>
</div>
@endsection


