@if(isset($category))
<?php $currentRoute = Route::currentRouteName().'/'.$category->id; ?>
<select id="categorySelect" class="form-control col-xs-2" onchange="currentCategory()">
	@foreach($categories as $category)
  		<option value="{{route('category',['id'=>$category->id])}}"{!! $currentRoute === Route::currentRouteName().'/'.$category->id ? 'selected' : ''!!}>
  		{{$category->name}}
  		</option>
  	@endforeach
  		<option value="{{route('news')}}"> All news</option>
</select>
@else
<select id="categorySelect" class="form-control col-xs-2" onchange="currentCategory()">
	@foreach($categories as $category)
  		<option value="{{route('category',['id'=>$category->id])}}">{{$category->name}}</option>
  	@endforeach
  		<option value="{{route('news')}}" selected> All news</option>
</select>
@endif

