<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Category;

class CategoryController extends Controller
{
    
/************** user functions ************************/
    public function getUserCategories()
    {   
        $categories = Category::all();
        return view('user-views.news',['categories'=>$categories]);
    }
    
    public function viewCategory($id)
    {   
        $category = Category::find($id);
        $categories = Category::all();
        return view('user-views.news',['category'=>$category,'categories'=>$categories]);
    }

/************** admin functions ***********************/
    public function getAdminCategories()
    {   
        $categories = Category::all();
        return view('admin-views.categories',['categories'=>$categories]);
    }

    public function deleteCategory($id)
    {   
        $category = Category::find($id);
        $category->delete();
        return redirect()->back()->with(['message'=>'The category "'.$category->name.'" was deleted']);
    }

    public function createCategory(Request $request)
    {	
        $this->validate($request,['name'=>'required|max:30']);
    	$category = new Category();
    	$category->name = $request->name;
    	$category->save();
        return redirect()->back()->with(['message'=>'New category created']);
    }

    public function editCategory($id)
    {	
    	$category = Category::find($id);
    	return view('admin-views.category-edit',['category'=>$category]);
    }

    public function getCategoryArticles($id)
    {   
        $categories = Category::all();
        $category = Category::find($id);
        return view('admin-views.articles',['category'=>$category,'categories'=>$categories]);
    }

   public function updateCategory(Request $request,$id)
    {	
        $this->validate($request,['name'=>'required|max:30']);
    	$category = Category::find($id);
    	$category->name = $request->name;
    	$category->save();
        return redirect()->route('categories')->with(['message'=>'The category was updated']);
    } 
}
