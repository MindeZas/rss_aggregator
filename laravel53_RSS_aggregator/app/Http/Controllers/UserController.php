<?php 
namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

	public function postSignUp(Request $request)
	{
		$this->validate($request,[
			'email'=>'required|email|unique:users',
			'name'=>'required|max:120',
			'password'=>'required|min:4'
			]);

		$email = $request['email'];
		$name = $request['name'];
		$password = bcrypt($request['password']);

		$user = new User();
		$user->email = $email;
		$user->name = $name;
		$user->password = $password;

		$user->save();

		Auth::login($user);

		return redirect()->route('categories');
	}

	public function postSignIn(Request $request)
	{
		$email = $request['email'];
		$password = $request['password'];

		$this->validate($request,[
			'email'=>'required',
			'password'=>'required'
		]);/**/

		if(Auth::attempt(['email'=>$request['email'], 'password'=>$request['password']])){
			return redirect()->route('categories');
		}
		else{
			return redirect()->back()->withErrors(['user and/or password do not match']);
		}
	}

	public function getLogout(){
		Auth::logout();
		return redirect()->route('signin');
	}

	public function getAccount(){
		return view('admin-views.account',['user'=>Auth::user()]);
	}

	public function putSaveAccount(Request $request){

		$this->validate($request, [
			'name'=>'required|max:120',
			'password'=>'required|min:4
			']);

		$user = Auth::user();
		$user->name = $request['name'];
		$user->password = bcrypt($request['password']);
		$user->update();
		$message = 'password changed';
		return redirect()->route('account')->with(['message'=>$message]);
	}

}