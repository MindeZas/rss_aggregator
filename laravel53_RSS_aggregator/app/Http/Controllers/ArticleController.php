<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Article;

class ArticleController extends Controller
{
    
/************** user functions ************************/
    public function getUserCategories()
    {   
        $categories = Category::all();
        return view('user-views.news',['categories'=>$categories]);
    }
    
    public function viewCategory($id)
    {   
        $category = Category::find($id);
        $categories = Category::all();
        return view('user-views.news',['category'=>$category,'categories'=>$categories]);
    }

/************** admin functions ***********************/
     public function getArticles()
    {   
        $articles = Article::all();
        return view('admin-views.articles',['articles'=>$articles]);
    }
   
    public function deleteArticle($id)
    {   
        $article = Article::find($id);
        $article->delete();
        return redirect()->back()->with(['message'=>'The article "'.$article->name.'" was deleted']);
    }

    public function createArticle(Request $request)
    {	
        $this->validate($request,[
            'name'=>'required|max:30',
            'url'=>'required'
            ]);
    	$article = new Article();
    	$article->name = $request->name;
        $article->category_id = $request->category_id;
        $article->url = $request->url;
    	$article->save();
        return redirect()->back()->with(['message'=>'New article was created']);
    }
    /**/

    public function editArticle($id)
    {	
    	$article = Article::find($id);
    	return view('admin-views.article-edit',['article'=>$article]);
    }
/*
    public function getCategoryArticles($id)
    {   
        $category = Category::find($id);
        return view('admin-views.category-articles',['category'=>$category]);
    }
*/
   public function updateArticle(Request $request,$id)
    {	
        $this->validate($request,['name'=>'required|max:30','url'=>'required']);
    	$article = Article::find($id);
    	$article->name = $request->name;
        $article->url = $request->url;
    	$article->save();
        return redirect()->route('category.articles',['id'=>$article->category_id])->with(['message'=>'The article was updated']);
    } 
}
