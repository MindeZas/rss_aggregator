```
before you start, be sure you have installed wamp or xampp server and composer on your machine.

download and extract the file;

start cmd inside the main folder (RSS_aggregator);

run "composer install";

create .env file and setup database connection in it;

run "php artisan key:generate" command;

start the server;

run "php artisan migrate" command;

run "php artisan serve" command;

open this link http://localhost:8000/signup in your browser;

sing up and add some links for eg.  http://www.delfi.lt/rss/feeds/daily.xml, **https://www.tv3.lt/rss/sarasas/0;

open http://localhost:8000 and you should see some feeds from rss providers
```